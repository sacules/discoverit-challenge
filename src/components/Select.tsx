import * as SelectPrimitive from "@radix-ui/react-select";
import {
  CheckIcon,
  ChevronDownIcon,
  ChevronUpIcon,
} from "@radix-ui/react-icons";
import "./Select.css";

interface Props {
  name: string;
  placeholder: string;
  items: {
    value: string;
    text: string;
  }[];
  value: string;
  onValueChange: (_val: string) => void;
}

const Select = ({ name, placeholder, items, value, onValueChange }: Props) => (
  <SelectPrimitive.Root name={name} value={value} onValueChange={onValueChange}>
    <SelectPrimitive.Trigger
      className="border-2 border-gray-300 rounded w-full text-left p-2 flex items-center justify-between"
      aria-label={name}
    >
      <SelectPrimitive.Value placeholder={placeholder} aria-label={value}>
        {value}
      </SelectPrimitive.Value>
      <SelectPrimitive.Icon>
        <ChevronDownIcon />
      </SelectPrimitive.Icon>
    </SelectPrimitive.Trigger>
    <SelectPrimitive.Portal>
      <SelectPrimitive.Content className="overflow-hidden bg-white rounded z-10 shadow">
        <SelectPrimitive.ScrollUpButton className="grid place-items-center h-6">
          <ChevronUpIcon />
        </SelectPrimitive.ScrollUpButton>
        <SelectPrimitive.Viewport className="p-2">
          {items.map((item, i) => (
            <SelectPrimitive.Item
              className="text-xl h-8 py-2 pl-8 flex items-center relative select-none odd:bg-gray-100"
              key={i}
              value={item.value}
            >
              <SelectPrimitive.ItemText>{item.text}</SelectPrimitive.ItemText>
              <SelectPrimitive.ItemIndicator className="absolute left-0 w-8 inline-grid place-items-center">
                <CheckIcon />
              </SelectPrimitive.ItemIndicator>
            </SelectPrimitive.Item>
          ))}
        </SelectPrimitive.Viewport>
        <SelectPrimitive.ScrollDownButton className="SelectScrollButton">
          <ChevronDownIcon />
        </SelectPrimitive.ScrollDownButton>
      </SelectPrimitive.Content>
    </SelectPrimitive.Portal>
  </SelectPrimitive.Root>
);

export default Select;
