import { ReactNode } from "react";

interface Props {
  action: ReactNode;
  title: string;
}

const Header = ({ action, title }: Props) => (
  <header className="bg-gray-300 grid grid-cols-[1fr_2fr_1fr] py-4 px-2 items-center">
    <div>{action}</div>
    <p className="text-center font-semibold">{title}</p>
    <div className="justify-self-end rounded-full bg-gray-500 px-2 py-1 text-sm font-semibold text-white">
      MS
    </div>
  </header>
);

export default Header;
