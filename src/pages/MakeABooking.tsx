import { useEffect, useReducer, useState } from "react";
import { Link } from "react-router-dom";
import wretch from "wretch";
import DatePicker from "react-datepicker";
import Select from "../components/Select";
import "react-datepicker/dist/react-datepicker.css";
import Header from "../components/Header";

const gradesApi = wretch(
  "https://static.healthforcego.com/grades.json",
).resolve((r) => r.json());

function generateTimeIntervals() {
  const intervals: string[] = [];

  for (let hour = 0; hour <= 23; hour++) {
    for (const minute of ["00", "30"]) {
      const timeString = `${hour.toString().padStart(2, "0")}:${minute}`;
      intervals.push(timeString);
    }
  }

  return intervals;
}

const timeRanges = generateTimeIntervals().map((ti) => ({
  text: ti,
  value: ti,
}));

type TimeRange = {
  start: string;
  end: string;
};

function MakeABooking() {
  const [grades, setGrades] = useState<string[]>([]);
  const [selectedGrade, setSelectedGrade] = useState("");
  const [selectedTimeRange, setSelectedTimeRange] = useReducer(
    (prevState: TimeRange, newState: TimeRange) => {
      const newTimeRange = { ...prevState, ...newState };

      if (newTimeRange.start > newTimeRange.end) {
        newTimeRange.end = newTimeRange.start;
      }

      return newTimeRange;
    },
    {
      start: timeRanges.find((tr) => tr.value === "10:00")!.value,
      end: timeRanges.find((tr) => tr.value === "11:30")!.value,
    },
  );
  const [dateStart, setDateStart] = useState(new Date());
  const [dateEnd, setDateEnd] = useState<Date | null>(null);

  const onDateChange = (dates: [Date | null, Date | null]) => {
    const [start, end] = dates;
    setDateStart(start!);
    setDateEnd(end);
  };

  useEffect(() => {
    const getGrades = async () => {
      const g = (await gradesApi.get()) as { grades: string[] };
      setGrades(g.grades);
    };

    getGrades();
  }, []);

  const searchParams = new URLSearchParams({
    grade: selectedGrade,
    datestart: dateStart.toISOString(),
    dateend: dateEnd ? dateEnd.toISOString() : "",
    timestart: selectedTimeRange.start,
    timeend: selectedTimeRange.end,
  });

  return (
    <div>
      <Header action="burger" title="Make a Booking" />
      <main className="p-2 font-medium grid gap-4">
        <div className="flex justify-between items-center">
          <h2 className="font-semibold text-purple-600 text-xl">
            Book from Scratch
          </h2>
          <button className="rounded border-2 border-blue-600 bg-blue-100 px-6 py-2 text-sm">
            Re-Book Staff
          </button>
        </div>
        <Select
          name="grade"
          value={selectedGrade}
          onValueChange={(val: string) => setSelectedGrade(val)}
          placeholder="Select a grade..."
          items={grades ? grades.map((g) => ({ text: g, value: g })) : []}
        />
        <div className="flex justify-between gap-2 items-center">
          <Select
            name="datebegin"
            value={selectedTimeRange.start}
            onValueChange={(val: string) =>
              setSelectedTimeRange({ start: val, end: selectedTimeRange.end })
            }
            placeholder="Select a start time"
            items={timeRanges}
          />
          <span className="text-2xl">⟶</span>
          <Select
            name="dateend"
            value={selectedTimeRange.end}
            onValueChange={(val: string) =>
              setSelectedTimeRange({ start: selectedTimeRange.start, end: val })
            }
            placeholder="Select a finish time"
            items={timeRanges}
          />
        </div>
        <label className="flex gap-2">
          <input type="checkbox" />
          Been Before
        </label>
        <button className="rounded border-2 border-blue-600 px-6 py-2 text-sm">
          Edit Default Settings (2 modified)
        </button>
        <DatePicker
          name="daterange"
          selected={dateStart}
          onChange={onDateChange}
          startDate={dateStart}
          endDate={dateEnd}
          monthsShown={2}
          selectsRange
          inline
        />
        <Link
          to={{
            pathname: "/confirm",
            search: searchParams.toString(),
          }}
          className="uppercase"
        >
          Create Bookings
        </Link>
      </main>
    </div>
  );
}

export default MakeABooking;
