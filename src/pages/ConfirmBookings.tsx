import { useLoaderData } from "react-router-dom";
import Header from "../components/Header";

// milliseconds within a second, within a minute, within an hour, within a day
const MS_IN_A_DAY = 1000 * 60 * 60 * 24;

interface DateRange {
  start: Date;
  end: Date;
}

interface TimeRange {
  start: string;
  end: string;
}

export function loader({ request }) {
  const url = new URL(request.url);

  const grade = url.searchParams.get("grade");
  const datestart = url.searchParams.get("datestart") ?? "";
  const dateend = url.searchParams.get("dateend") ?? "";
  const timestart = url.searchParams.get("timestart");
  const timeend = url.searchParams.get("timeend");

  return {
    grade,
    dateRange: { start: new Date(datestart), end: new Date(dateend) },
    timeRange: { start: timestart, end: timeend },
  };
}

function calculateDays(dateRange: DateRange) {
  const timeBetweenDates = dateRange.end.getTime() - dateRange.start.getTime();
  const days = Math.round(timeBetweenDates / MS_IN_A_DAY);

  return days;
}

function generateBookings(dateRange: DateRange) {
  const totalDays = calculateDays(dateRange);
  const bookings = [];

  for (let i = 0; i < totalDays; i++) {
    const newDate = new Date(dateRange.start);
    newDate.setDate(dateRange.start.getDate() + i);

    const b = {
      person: "Dorothy McHardy",
      available: true,
      date: newDate,
    };

    bookings.push(b);
  }

  return bookings;
}

const ConfirmBookings = () => {
  const { grade, dateRange, timeRange } = useLoaderData();

  const bookings = generateBookings(dateRange);

  return (
    <div>
      <Header action="<" title="Accept Bookings" />
      <main>
        <h2>Bookings to be Confirmed</h2>
        <form>
          {bookings.map((b, i) => (
            <div key={i}>
              <p>
                {b.date.toLocaleDateString("en-us", {
                  weekday: "short",
                  day: "numeric",
                  month: "short",
                  year: "numeric",
                })}
              </p>
              <p>
                {b.available
                  ? `${b.person} is available`
                  : "Suitable staff member will be assigned"}
              </p>
              <p>
                {timeRange.start} - {timeRange.end}
              </p>
            </div>
          ))}

          <button type="submit">Confirm Bookings</button>
        </form>
      </main>
    </div>
  );
};

export default ConfirmBookings;
