import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider, createBrowserRouter } from "react-router-dom";

import MakeABooking from "./pages/MakeABooking";
import ConfirmBookings, {
  loader as confirmBookingsLoader,
} from "./pages/ConfirmBookings";
import "./index.css";

const router = createBrowserRouter([
  {
    path: "/",
    element: <MakeABooking />,
  },
  {
    path: "/confirm",
    element: <ConfirmBookings />,
    loader: confirmBookingsLoader,
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
);
